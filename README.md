Install the project's dependencies in the local node_modules directory:

    npm install

Install webpack globally:

    npm install -g webpack
    
Install webpack-dev-server globally:

    npm install -g webpack-dev-server

Run webpack-dev-server:

    webpack-dev-server --port 8080 --hot --host 0.0.0.0

In a different terminal window execute:

    node server

The app can be found on: http://localhost:8080/app/